#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../util.h"

void
handle_button(int button)
{
	switch (button) {
	case 1: 
		system("cmus-remote -u");
		break;
	case 3: 
		system("nowplaying");
		break;
	case 4: 
		system("cmus-remote -n");
		break;
	case 5: 
		system("cmus-remote -r");
		break;
	case 6: 
		system("cmus-remote -n");
		break;
	case 7: 
		system("cmus-remote -r");
		break;
	}
}

char*
music(int button)
{
	FILE *f;
	char *line = NULL;
	size_t linelen = 0;
	static char str[STATUSLEN];	

	handle_button(button);

	f = fopen("/tmp/cmus-music", "r");

	if (f == NULL)
		return "";

	getline(&line, &linelen, f);

	fclose(f);
	strcpy(str, line);
	free(line);

	return str;
}
