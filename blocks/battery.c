#include <stdio.h>
#include <string.h>
#include <string.h>
#include "../util.h"

#define BATT_CAPACITY "/sys/class/power_supply/BAT0/capacity"
#define BATT_STATUS   "/sys/class/power_supply/BAT0/status"

#define ICON1 ""
#define ICON2 ""
#define ICON3 ""
#define ICON4 ""
#define ICON5 ""
#define ICON6 ""
#define ICON7 ""

void
panic(char* msg)
{
	fprintf(stderr, "%s\n", msg);
}

char*
get_status()
{
	static char status[255];
	FILE *fstat;

	fstat = fopen(BATT_STATUS, "r");
	if (fstat == NULL) {
		panic("could not open " BATT_STATUS);
		return "";
	}
	if (fscanf(fstat, "%s", status) > 1) {
		panic("cannot read " BATT_STATUS); 
		return "";
	}
	fclose(fstat);

	if (!strcmp(status, "Charging"))
		return "";
	return "";
}

char*
battery(int button)
{
	static char str[STATUSLEN];
	char  icon[4];
	int capacity;
	FILE *fcap;

	fcap = fopen(BATT_CAPACITY, "r");
	if (fcap == NULL) {
		panic("could not open " BATT_CAPACITY);
		return "";
	}

	if (fscanf(fcap, "%d", &capacity) < 1) {
		panic("cannot read " BATT_CAPACITY); 
		return "";
	}

	fclose(fcap);

	if (capacity > 96) {
		capacity = 100;
		strncpy(icon, ICON7, LEN(icon));
	}
	else if (capacity > 85)
		strncpy(icon, ICON6, LEN(icon));
	else if (capacity > 58)
		strncpy(icon, ICON5, LEN(icon));
	else if (capacity > 51)
		strncpy(icon, ICON4, LEN(icon));
	else if (capacity > 34)
		strncpy(icon, ICON3, LEN(icon));
	else if (capacity > 17)
		strncpy(icon, ICON2, LEN(icon));
	else
		strncpy(icon, ICON1, LEN(icon));

	snprintf(str, STATUSLEN, "%s %d%%%s",icon, capacity, get_status());

	return str;
}
