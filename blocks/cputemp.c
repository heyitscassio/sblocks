#include <stdio.h>
#include "../util.h"

#define CPUTHERMALS "/sys/class/thermal/thermal_zone0/temp"

#define ICON ""

char*
cputemp(int button)
{
	FILE *f;	
	static char str[STATUSLEN];
	int temp;

	f = fopen(CPUTHERMALS, "r");
	if (f == NULL) {
		fprintf(stderr, "could not open " CPUTHERMALS);
		return "";
	}
	if (fscanf(f, "%d", &temp) > 1) {
		fprintf(stderr, "could not read " CPUTHERMALS);
		return "";
	}
	fclose(f);

	sprintf(str, ICON " %d°C", temp / 1000);
	return str;
}
