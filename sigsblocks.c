#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

#define STATUSBAR "sblocks"

pid_t statuspid; 

pid_t
getstatusbarpid()
{
	char buf[32], *str = buf, *c;
	FILE *fp;

	if (statuspid > 0) {
		snprintf(buf, sizeof(buf), "/proc/%u/cmdline", statuspid);
		if ((fp = fopen(buf, "r"))) {
			fgets(buf, sizeof(buf), fp);
			while ((c = strchr(str, '/')))
				str = c + 1;
			fclose(fp);
			if (!strcmp(str, STATUSBAR))
				return statuspid;
		}
	}
	if (!(fp = popen("pidof -s "STATUSBAR, "r")))
		return -1;
	fgets(buf, sizeof(buf), fp);
	pclose(fp);
	return strtol(buf, NULL, 10);
}

int
main(int argc, char**argv)
{
    union sigval val;

	if (argc < 3) {
		fprintf(stderr, "Usage %s SIGNAL BUTTON\n", argv[0]);
		return 1;
	}

	val.sival_int = atoi(argv[2]);

    sigqueue(getstatusbarpid(), SIGRTMIN + atoi(argv[1]), val);
}
