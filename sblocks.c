#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <X11/Xlib.h>

#include "util.h"

void setup();
void setupx();
void sighandler(int signum, siginfo_t *si, void *ucontext);
void statusloop();
void writepid();
void runblock(const Block *block, char *out, int button);
void updateblocks(int time);

static char statusstr[LEN(blocks)][STATUSLEN + DELIMLEN];
char fullstatus[2][STATUSLEN + DELIMLEN];

static Window root;
static Display *dpy;

void
setup()
{
	struct sigaction sa;

	sa.sa_sigaction = sighandler;
	sa.sa_flags = SA_SIGINFO;

	for (int i = 0; i < LEN(blocks); i++)
		sigaction(SIGRTMIN + blocks[i].signal, &sa, NULL);
}

void
setupx()
{
	static int screen;
	dpy = XOpenDisplay(NULL);
	if (!dpy) {
		fprintf(stderr, "sblocks: Failed to open display\n");
		exit(1);
	}
	screen = DefaultScreen(dpy);
	root = RootWindow(dpy, screen);
}

void
sighandler(int signum, siginfo_t *si, void *ucontext)
{
	int signal = signum - SIGRTMIN;
	int button = si->si_value.sival_int; /* if button is zero, the signal 
                                         is not from a button press */

	for (int i = 0; i < LEN(blocks); i++)
		if (signal == blocks[i].signal)
			runblock(&blocks[i], statusstr[i], button);
}

void
statusloop()
{
	int timer = 0;
	updateblocks(-1);
	while (1) {
		updateblocks(timer++);
		strcpy(fullstatus[0], "\0");

		for (int i = 0; i < LEN(statusstr); i++) {
			strcat(fullstatus[0], statusstr[i]);
			if (i != LEN(statusstr) - 1)
				strcat(fullstatus[0], DELIM);
		}

		/* store name only if it changed */
		if (strcmp(fullstatus[0], fullstatus[1]) != 0) {
			XStoreName(dpy, root, fullstatus[0]);
			XFlush(dpy);
		}

		strcpy(fullstatus[1], fullstatus[0]);
		sleep(1.0);
	}
}

void
writepid()
{
	FILE *fp;

	fp = fopen("/tmp/sblocks.pid", "w");
	fprintf(fp, "%d\n", getpid());

	fclose(fp);
}

void
updateblocks(int time)
{
	for (int i = 0; i < LEN(blocks); i++)
		if ((blocks[i].interval != 0 && time % blocks[i].interval == 0) || time == -1)
			runblock(&blocks[i], statusstr[i], 0);
}

void
runblock(const Block *block, char *out, int button)
{
#ifdef CLICKABLE
	if (block->signal)
		*out++ = block->signal;
#endif

	strcpy(out, block->func(button));
	out[strlen(out) + 1] = '\0';
}

int
main()
{
	setupx();
	writepid();
	setup();
	statusloop();

	return 0;
}
