# sblocks

sblocks is a simple status monitor for dwm. 

It is written and configured in c. And it supports mouse clicks by default, you just need
to apply the [statuscmd](https://dwm.suckless.org/patches/statuscmd/) patch to dwm
and change `#define STATUSBAR "dwmblocks"` to `#define STATUSBAR "sblocks"` in
the config.def.h.

## Installing

```sh
$ make
# make install
```

Or you can change `DESTIR` or `PREFIX` to suit your needs. By default it
installs into `/usr/local/bin`

## Creating a new block

To create a new block you need to declare it in `blocks/blocks.h` first, it
should always be a `char*` and take a `int` as an argument.

```c
char* battery(int button);
char* calendar(int button);
char* cputemp(int button);
char* volume(int button);
char* example(int button);
```

Now create a file at `blocks/example.c`, with the function `char* example(int button);`

```c
char*
example(int button)
{
	// do whatever you want here
	return "this is an example";
}
```

To add it to the status monitor, open `config.h` and add it to the `blocks[]`
array:

```c
static const Block blocks[] = {
	// function delay signal
	{battery,   20,   1 },
	{volume,    30,   2 },
	{cputemp,   10,   3 },
	{calendar,  30,   4 },
	{example,   -1,   5 },
};
```

If you don't want to update the block ever, you can use a delay of `-1`. Or if
you want to update it every time when possible, you can use a delay of `0`, be
aware this can be very memory and cpu expensive.
