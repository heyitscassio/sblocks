#include <alsa/asoundlib.h>
#include <alsa/mixer.h>
#include <stdio.h>
#include <string.h>
#include "../util.h"

#define CARD      "default"
#define SELEMNAME "Master"

#define ICON_MUTE ""
#define ICON_LOW  ""
#define ICON_MID  ""
#define ICON_HIGH ""

snd_mixer_t *handle = NULL;
snd_mixer_selem_id_t *sid = NULL;

void
change_volume(long n)
{
	long max, min, current;
    snd_mixer_elem_t* elem;

	elem = snd_mixer_find_selem(handle, sid);

    snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
	snd_mixer_selem_get_playback_volume(elem, 0, &current);

	if (current + n > max) {
		return;
	}
		
    snd_mixer_selem_set_playback_volume_all(elem, current + n);
}

void
initalsa()
{
    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, CARD);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);
}

long
map(long n, long in_min, long in_max, long out_min, long out_max)
{
	return (n - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void
toggle_mute()
{
    snd_mixer_elem_t* elem;
	int state;

	elem = snd_mixer_find_selem(handle, sid);

	snd_mixer_selem_get_playback_switch(elem, 0, &state);

	snd_mixer_selem_set_playback_switch(elem, 0, state ^ 1);
}

char*
volume(int button)
{
	static char str[STATUSLEN];
	static char icon[4];

    long vol, min, max, pcent;
	int state;

    snd_mixer_elem_t* elem;

	initalsa();

    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, SELEMNAME);

	elem = snd_mixer_find_selem(handle, sid);

	switch (button) {
	case 0:
		break;
	case 1:
		toggle_mute();
		break;
	case 2:
	case 3:
		break;
	case 4:
		change_volume(1);
		break;
	case 5:
		change_volume(-1);
		break;
	default:
		break;
	}

    snd_mixer_selem_get_playback_volume_range(elem, &min, &max);

	snd_mixer_selem_get_playback_volume(elem, 0, &vol);
	snd_mixer_selem_get_playback_switch(elem, 0, &state);

	if (state) {
		pcent = map(vol, min, max, 0, 100);

		if (pcent > 70)
			strcpy(icon, ICON_HIGH);
		else if (pcent < 30)
			strcpy(icon, ICON_LOW);
		else
			strcpy(icon, ICON_MID);

		sprintf(str, "%s %2ld%%", icon, map(vol, min, max, 0, 100));
	}
	else 
		sprintf(str, "%s muted", ICON_MUTE);

    snd_mixer_close(handle);
	return str;
}
