#include "blocks/blocks.h"

typedef struct {
	char* (*func)(int button);
	unsigned int interval;
	unsigned int signal;
} Block;

#define STATUSLEN 255

#define DELIM " "
#define DELIMLEN 3

/* Comment if you don't want clickable blocks */
#define CLICKABLE

static const Block blocks[] = {
	// function delay signal
	{music,     20,   5 },
	{battery,   20,   4 },
	{volume,    30,   3 },
	{cputemp,   10,   2 },
	{calendar,  30,   1 },
};
