include config.mk

SRC = sblocks.c $(wildcard blocks/*c)
OBJ = ${SRC:.c=.o}

all: sblocks sigsblocks

${OBJ}: config.h config.mk 

sblocks: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

sigsblocks sigsblocks.c:
	${CC} sigsblocks.c -o $@

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f sblocks ${DESTDIR}${PREFIX}/bin
	cp -f sigsblocks ${DESTDIR}${PREFIX}/bin

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/sblocks ${DESTDIR}${PREFIX}/bin/sigsblocks

clean:
	rm -f sblocks sigsblocks ${OBJ}

