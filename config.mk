PREFIX = /usr/local

LIBS = -lX11 -lasound

CFLAGS   += -std=gnu99 -pedantic -Wall -Wno-deprecated-declarations -Os
LDFLAGS  += ${LIBS}

CC = cc
