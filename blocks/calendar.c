#include <stdio.h>
#include <time.h>
#include "../util.h"

#define CALICON ""
#define CLOCKICON ""

short display = 0;

enum mode {
	TIME,
	FULL,
	NUMBER,
};

char*
calendar(int button)
{
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	static char str[STATUSLEN];

	switch (button) {
	case 1:
		display = display != FULL ? FULL : TIME;
		break;
	case 3:
		display = display != NUMBER ? NUMBER : TIME;
		break;
	}

	switch (display) {
	case TIME:
    	strftime(str, STATUSLEN, CLOCKICON " %R", &tm);
		break;
	case FULL:
    	strftime(str, STATUSLEN, CALICON " %a, %b %d", &tm);
		break;
	case NUMBER:
    	strftime(str, STATUSLEN, CALICON " %d/%m/%y", &tm);
		break;
	}

	return str;
}
